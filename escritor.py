import pandas as pd
import json as js

dataSetTabela = {
    'nome':['alan', 'jose', 'mario'],
    'telefone':['0000-00000', '1111-11111', '2222-22222'],
    'email':['alan@gmail.com', 'jose@gmail.com', 'mario@gmail.com'],
    'idade':[45, 42, 80]
}

# criação do dataframe
tabela = pd.DataFrame(dataSetTabela,columns=['nome','telefone','email','idade'])

# renomeando colunas da tabela
tabela = tabela.rename(columns={'nome':'NomeCliente','telefone':'TelefoneCliente','email':'EmailCliente','idade':'IdadeCliente'})

# geração de json file
json = tabela.to_json(path_or_buf='temp.json', orient='records', lines=False, indent=2)

# geração de excel file
excel = tabela.to_excel("temp.xlsx",index=False)

# geração de json para exibir no html
records = js.loads(tabela.to_json(orient='records'));

print(records)





