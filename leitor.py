import pandas as pd
import json as jsn

# ler o arquivo excel
excelSet = pd.read_excel('temp.xlsx')

# seleciona as colunas
excel=pd.DataFrame(excelSet,columns=['NomeCliente','TelefoneCliente'])

# printa as colunas
print(excel)

# geração de excel file
saida = excel.to_json(path_or_buf="saida.json", orient='records', lines=False, indent=2)